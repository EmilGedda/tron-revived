package com.tron.sound;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class SoundHandler {
    private static SoundHandler singleton;
    private static Music menu;
    private static Music ingame;
    private static Music end;

    public static SoundHandler GetHandler(){
        if(singleton == null)
            singleton = new SoundHandler();
        return singleton;
    }

    public SoundHandler() {
        try {
            menu = new Music("Resources/audio/17 - Disc Wars.ogg", true);
            ingame = new Music("Resources/audio/13 - Derezzed.ogg", true);
            end = new Music("Resources/audio/EOL.ogg", true);
        } catch (SlickException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        singleton = this;
       // PlayMusic(Track.MENU);
    }

    public enum Track {
        MENU,
        GAME,
        END
    };

    public enum SoundEffect {
        POWERUP, POWERDOWN,
        BOOST, FIRE,
        HIT, CRASH,
        GAMECOUNTDOWN, GAMEGO
    };

    Track current;
    Music nowPlaying;

    public void PlayMusic(Track music) {
        if(music == current) return;
        current = music;
        switch(music) {
        case MENU:
            if(ingame.playing())
                ingame.pause();
            menu.loop();
            break;
        case GAME:
            if(menu.playing())
                menu.pause();
            if(end.playing())
                end.pause();
            ingame.loop();
            break;
        case END:
            if(ingame.playing())
                ingame.pause();
            end.loop();
        }
    }

    public void StopMusic() {
        nowPlaying.stop();
    }

    public void PauseMusic() {
        nowPlaying.pause();
    }

    public void PlaySound(SoundEffect sound) {
        try{
            Sound soundfx = null;
            switch(sound) {
            case POWERUP:
                soundfx = new Sound("Resources/audio/powerup.wav");
                break;
            case POWERDOWN:
                soundfx = new Sound("Resources/audio/powerdown.wav");
                break;
            case BOOST:
                soundfx = new Sound("Resources/audio/boost.wav");
                break;
            case FIRE:
                soundfx = new Sound("Resources/audio/fire.wav");
                break;
            case HIT:
                soundfx = new Sound("Resources/audio/hit.ogg");;
                break;
            case CRASH:
                soundfx = new Sound("Resources/audio/crash.ogg");
                break;
            case GAMECOUNTDOWN:
                soundfx = new Sound("Resources/audio/gamecountdown.ogg");
                break;
            case GAMEGO:
                soundfx = new Sound("Resources/audio/gamego.ogg");
                break;
            }
            soundfx.play();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
