package com.tron.states;

import java.util.Stack;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.sound.SoundHandler;

public class StateHandler {

    public enum State {
        StartMenu, OptionsMenu, PauseMenu, Singleplayer, Multiplayer, PlayerSelection,
        GameEnd
    }

    private static StateHandler singleton;
    static SoundHandler soundHandler = new SoundHandler();
    static Stack<State> stateStack = new Stack<>();
    static State currentState = State.StartMenu;


    public static StateHandler GetHandler(){
        if(singleton == null)
            singleton = new StateHandler();
        return singleton;
    }

    public void changeState(State state, StateBasedGame game) {
        switch(state) {
        case StartMenu:
            soundHandler.PlayMusic(SoundHandler.Track.MENU);
            break;
        case OptionsMenu:
            soundHandler.PlayMusic(SoundHandler.Track.MENU);
            break;
        case PauseMenu:
            soundHandler.PauseMusic();
            break;
        case Singleplayer:
            soundHandler.PlayMusic(SoundHandler.Track.GAME);
            break;
        case Multiplayer:
            soundHandler.PlayMusic(SoundHandler.Track.GAME);
        case GameEnd:
            soundHandler.PlayMusic(SoundHandler.Track.END);
        }
        game.enterState(state.ordinal());
        push(currentState);
        currentState = state;
    }

    public void back(StateBasedGame game) throws SlickException {
        if(stateStack.size() > 0) {
            changeState(pop(), game);
        }
    }

    public void push(State state) {
        stateStack.push(state);
    }

    public State pop() {
        return stateStack.pop();
    }
}
