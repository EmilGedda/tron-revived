package com.tron.states;

import java.util.Arrays;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.entities.Player;
import com.tron.menu.Menu;
import com.tron.menu.MenuItem;
import com.tron.menu.TextItem;

public class GameEndState extends Menu{
    private Player winner = null;
    private Image img;
    private Vector2f pos = new Vector2f(900, 700);
    private MenuItem item;
    private float rot;

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        winner = LocalGame.getWinner();
        img = new Image("Resources/bike_" + winner.getColor().toString()+".png").getScaledCopy(2);
        TextItem titem = TextItem.defaultMenuItem(winner.getColor().toString() + " won!");
        titem.setFontSize(70);
        item = new MenuItem(titem, 800, 400);
        item.setX(1920/2F - item.getWidth()/2);
        pos.set(1920/2F, 580);
        items.add(item);
        super.enter(container, game);
    }
    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);
        MenuItem[] itm = new MenuItem[]{
                new MenuItem(TextItem.defaultMenuItem("Play again"), (a, b, c) -> {
                    items.remove(item);
                    StateHandler.GetHandler().changeState(StateHandler.State.Singleplayer, game);
                }, center + 30, 690),

                new MenuItem(TextItem.defaultMenuItem("Menu"), (a, b, c) -> {
                    items.remove(item);
                    StateHandler.GetHandler().changeState(StateHandler.State.StartMenu, game);
                }, center + 310, 690) 
        };
        items.addAll(Arrays.asList(itm));
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        super.render(container, game, g);
        img.drawCentered(pos.x, pos.y);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {    
        super.update(container, game, delta);
        rot = delta/60F;
        img.rotate(rot);
    }

    @Override
    public int getID() {
        // TODO Auto-generated method stub
        return StateHandler.State.GameEnd.ordinal();
    }

}
