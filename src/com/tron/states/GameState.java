package com.tron.states;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.entities.Background;
import com.tron.entities.Explosion;
import com.tron.entities.Player;
import com.tron.entities.PowerUp;
import com.tron.entities.PowerUpFactory;

public class GameState extends BasicGameState {

    ArrayList<Player> players;
    protected static ArrayList<Explosion> explosions = new ArrayList<Explosion>();
    Background background = Background.CreateBackground("Resources/background.png");    
    ArrayList<Rectangle> walls;
    ArrayList<PowerUp> powerups;
    PowerUpFactory factory;

    public static void addExplosion(float x, float y){
        explosions.add(new Explosion(x, y));
    }
    
    @Override
    public int getID() {
        return StateHandler.State.Singleplayer.ordinal();
    }

    @Override
    public void init(GameContainer arg0, StateBasedGame arg1) {
        players = new ArrayList<Player>();
        walls = new ArrayList<Rectangle>();
        factory = new PowerUpFactory();
        powerups = new ArrayList<PowerUp>();
        walls.add(new Rectangle(10, 10, 20, 1080 - 20));
        walls.add(new Rectangle(10, 10, 1920 - 20, 20));
        walls.add(new Rectangle(1920 - 10 - 20, 10, 20, 1080 - 20));
        walls.add(new Rectangle(10, 1080 - 10 - 20, 1920 - 20, 20));
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        background.draw(container, game, g);
        
        for(Player p : players)
            p.render(container, game, g);
        
        for(PowerUp p : powerups)
            p.render(container, game, g);
        
        Color clr = g.getColor();
        g.setColor(Color.lightGray);
        
        for(Rectangle r : walls)
            g.draw(r);
        
        for(Explosion e : explosions)
            e.render();
        
        g.setColor(clr);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if(!container.hasFocus()) return;
        for(Explosion e : explosions)
            e.update(delta);
        factory.update(delta);
        
        if(factory.isReady()){
            powerups.add(factory.Random());
        }
        for(PowerUp p : powerups)
            p.update(container, game, delta);
        
        Iterator<Player> iter = players.iterator();
        while(iter.hasNext()){
            Player p = iter.next();
            p.update(container, game, delta);
            if(p.isAlive()){
                Iterator<PowerUp> it = powerups.iterator();
                
                while(it.hasNext()){
                    PowerUp power = it.next();
                    if(power.getHitbox().intersects(p.getHitbox())){
                        p.pickUpPowerUp(power);
                        it.remove();
                    }
                }
                
                for(Player p2 : players)
                    if(p.collidesWith(p2))
                        p.Kill();
                    
                for(Rectangle r : walls)
                    if(p.collidesWithWall(r))
                        p.Kill();
                    
                
            } else iter.remove(); 
        }
    }
}
