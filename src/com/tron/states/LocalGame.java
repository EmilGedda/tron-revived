package com.tron.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.entities.Explosion;
import com.tron.entities.Player;
import com.tron.entities.Player.PlayerColor;
import com.tron.menu.PlayerSelection;
import com.tron.menu.PlayerSelection.InputType;
import com.tron.menu.TextItem;
import com.tron.sound.SoundHandler;
import com.tron.sound.SoundHandler.SoundEffect;

public class LocalGame extends GameState {

	private boolean running = false;
	private int counter = 5000;
	private Vector2f counterpos;
	private Color overlay;
	private Color black = Color.black;
	private TextItem countback;
	private TextItem countfront = TextItem.defaultMenuItem("5.000");
	private static Player winner = null;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		powerups.clear();
		PlayerColor[] colors = new PlayerColor[]{PlayerColor.CYAN, PlayerColor.ORANGE, PlayerColor.GREEN, PlayerColor.MAGENTA};
		InputType[] inputs = PlayerSelection.getSelectedInput();
		players.clear();
		for(int i = 0; i < inputs.length; i++)
			if(inputs[i] != InputType.OFF)
				players.add(Player.CreatePlayer(colors[i], container, inputs[i]));
	}

	public static Player getWinner(){
		return winner;
	}

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) {
		countback  = TextItem.defaultMenuItem("5.000");
		countfront = TextItem.defaultMenuItem("5.000");
		overlay = new Color(0, 0, 0, 1F); 
		countfront.setFontSize(120);
		countback.setFontSize(120);
		countfront.outline(5, java.awt.Color.WHITE);   
		counterpos = new Vector2f(1920/2F - countfront.getWidth()/2, 1080/2F - countfront.getHeight()/2);
		super.init(arg0, arg1);        
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) {
		super.render(container, game, g);
		if(!running){
			Color clr = g.getColor();
			overlay.a = counter/5000F;
			g.setColor(overlay);
			g.fillRect(0, 0, 1920, 1080);

			g.setColor(clr);
			if(counter > 0 && players.size() > 1){
				countback.draw(counterpos);
				countfront.draw(counterpos,black);
			}
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		if(players.size() == 1){
			for(Explosion e : explosions)
				e.update(delta);
			running = false;
			winner = players.get(0);
			counter += 2*delta;            
			if(counter > 5000)
				StateHandler.GetHandler().changeState(StateHandler.State.GameEnd, game);
		}        
		else if(counter >= 0){
			if(counter - delta >= 0 && (counter % 1000) < delta)
				SoundHandler.GetHandler().PlaySound(SoundEffect.GAMECOUNTDOWN);
			counter -= delta;
			String str = Float.toString(counter/1000F);
			countfront.setText(str);
			countback.setText(str);
			container.getInput().clearKeyPressedRecord();
		}
		else{
			if(!running){
				SoundHandler.GetHandler().PlaySound(SoundEffect.GAMEGO);
				running = true;
			}
			super.update(container, game, delta);
		}
	}
}