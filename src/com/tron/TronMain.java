package com.tron;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.config.ConfigProperty;
import com.tron.config.GameConfig;
import com.tron.states.GameEndState;
import com.tron.states.LocalGame;
import com.tron.menu.OptionsMenu;
import com.tron.menu.PlayerSelection;
import com.tron.menu.StartMenu;
import com.tron.states.StateHandler;


public class TronMain extends StateBasedGame {
	
	StateHandler stateHandler;

    public static void main(String[] args) {
        try {
            GameConfig cfg = GameConfig.getConfig();
            AppGameContainer appgc = new AppGameContainer(new ScalableGame(
                    new TronMain("Tron - Revived"), 1920, 1080, false));
            appgc.setDisplayMode(
                    cfg.getProperty(ConfigProperty.RESOLUTIONWIDTH),
                    cfg.getProperty(ConfigProperty.RESOLUTIONHEIGHT),
                    cfg.getProperty(ConfigProperty.FULLSCREEN) == 1 ? true : false);
            appgc.setTargetFrameRate(120);
            appgc.start();
           // appgc.getGraphics().setAntiAlias(true);
        } catch (SlickException ex) {
            Logger.getLogger(TronMain.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public TronMain(String gamename) {
        super(gamename);
    }

    @Override
    public boolean closeRequested() {
        Logger.getLogger(TronMain.class.getName()).log(Level.INFO,
                "Game exiting, saving config.");
        GameConfig.getConfig().save();
        return true;
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
    	stateHandler = new StateHandler();
        addState(new StartMenu());
        addState(new OptionsMenu());
        addState(new LocalGame());
        addState(new PlayerSelection());
        addState(new GameEndState());
    }
}