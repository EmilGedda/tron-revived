package com.tron.entities;

import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.tron.entities.PowerUp.Type;
import com.tron.sound.SoundHandler;
import com.tron.sound.SoundHandler.SoundEffect;

public class PowerUpFactory {

    int time;
    boolean ready;
    Random rand;
    
    public enum Effect {
        TURN,
        SPEEDUP,
        SPEEDDOWN,
        INVISWALLS
    }
    
    private Effect[] effects = Effect.values();
    public PowerUpFactory() {
        rand = new Random();
        time = rand.nextInt(4000) + 1000;
    }

    public PowerUp Random(){
        return Create(effects[rand.nextInt(effects.length)]);
    }

    public boolean isReady(){
        return ready;
    }
    private void playEffect(SoundEffect fx){
        SoundHandler.GetHandler().PlaySound(fx);
    }
    public PowerUp Create(Effect fx){	
        ready = false;
        switch (fx){
        case TURN:
            return Turn(rand.nextInt(1800) + 100, rand.nextInt(900) + 50, SoundEffect.POWERDOWN);
        case SPEEDUP:
            return Speed(rand.nextInt(1800) + 100, rand.nextInt(900) + 50, 2F, SoundEffect.POWERUP);
        case SPEEDDOWN:
            return Speed(rand.nextInt(1800) + 100, rand.nextInt(900) + 50, -1F, SoundEffect.POWERDOWN);
        case INVISWALLS:
            return InvisibleWalls(rand.nextInt(1800) + 100, rand.nextInt(900) + 50, SoundEffect.POWERUP);
        }
        return null;
    }	

    public void update(int delta){
        time -= delta;
        if(time <= 0){
            ready = true;
            time = rand.nextInt(4500) + 1500;
        }
    }

    public PowerUp Turn(float x, float y, SoundEffect fx){
        PowerUpInterface action = new PowerUpInterface() {
            Random rng = new Random();
            @Override
            public void before(Player victim, int delta) {
                playEffect(fx);
            }

            @Override
            public void update(Player victim, int delta) {   
                if(rng.nextFloat() < delta*0.1F/45)
                    victim.setDirection(victim.getDirection() + rng.nextInt(3) - 1);
            }
        };
        Image img = null;
        try {
            img = new Image("Resources/turn.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
        return new PowerUp(new ImageEntity(img), Type.NEGATIVE, x, y, 2000, action);
    }

    public PowerUp InvisibleWalls(float x, float y, SoundEffect fx){
        PowerUpInterface action = new PowerUpInterface() {
            @Override
            public void before(Player victim, int delta) {
                playEffect(fx);
                victim.getTail().hide(); 
            }

            @Override
            public void after(Player victim, int delta) {
                victim.getTail().show();
            }
        };
        Image img = null;
        try {
            img = new Image("Resources/invis.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
        return new PowerUp(new ImageEntity(img), Type.POSITIVE, x, y, 2000, action);
    }
    public PowerUp Speed(float x, float y, float speeddiff, SoundEffect fx){
        PowerUpInterface action = new PowerUpInterface() {
            @Override
            public void before(Player victim, int delta) {
                playEffect(fx);
                victim.setSpeed(victim.getSpeed() + speeddiff); 
            }

            @Override
            public void after(Player victim, int delta) {
                victim.setSpeed(victim.getSpeed() - speeddiff);
            }
        };
        Image img = null;
        try {
            img = new Image(speeddiff < 0 ? "Resources/arrow_slow.png" : "Resources/arrow_speed.png");
        } catch (SlickException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new PowerUp(new ImageEntity(img), speeddiff > 0 ? Type.POSITIVE : Type.NEGATIVE, x, y, 2000, action);
    }
}
