package com.tron.entities;

import java.util.ArrayList;

import javax.management.RuntimeErrorException;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import com.tron.entities.Player.PlayerColor;

public class Tail {
    private int direction;
    private Color color;
    private boolean visible = true;
    private ArrayList<Rectangle> tail = new ArrayList<Rectangle>();

    public Tail(float centerX, float centerY, int direction, PlayerColor color) {
        this.direction = direction;
        this.color = getSlickColor(color);
        addRectangle(centerX, centerY);
    }
    
    public void hide(){
        visible = false;
    }
    
    public void show(){
        visible = true;
    }
    public void clear(){
        tail.clear();
    }
    private void addRectangle(float x, float y) {
        Rectangle rect = new Rectangle(x, y, 5, 5);
        rect.setCenterX(x);
        rect.setCenterY(y);
        tail.add(rect);
    }

    private Rectangle current() {
        return tail.get(tail.size() - 1);
    }

    public void draw(Graphics g) {
        if(!visible) return;
        Color clr = g.getColor();
        g.setColor(color);
        for (Rectangle rect : tail)
            g.draw(rect);
        g.setColor(clr);
    }
    public void explode(Player owner, Rocket rocket){
        Rectangle rect = getCollider(rocket.getHitbox()); 
        float width, height;
        Rectangle nrect = new Rectangle(0, 0, 0, 0);
        nrect.setBounds(rect);
        boolean add =true;
        if(rect.getWidth() < rect.getHeight()){
            width = rect.getWidth();
            height = rocket.getY() - rect.getY() - 30;
            if(rect.getHeight() - height - 60 > 0){
                nrect.setHeight(rect.getHeight() - height - 60);
                nrect.setY(rect.getY() + height + 60);
            } else add = false;
        } else {
            width = rocket.getX() - rect.getX() - 30;
            height = rect.getHeight();
            if(rect.getWidth() - width - 60 > 0){
                nrect.setWidth(rect.getWidth() - width - 60);
                nrect.setX(rect.getX() + width + 60);
            } else add = false;

        }
        int idx = tail.indexOf(rect);    
        if(owner.getDirection() == Player.EAST || owner.getDirection() == Player.SOUTH)
            idx += 1;
        if(width < 0 || height < 0){
            tail.remove(rect);
            idx--;
        }
        if(idx < 0) idx = 0;
        else rect.setWidth(width); rect.setHeight(height);
        if(add || idx >= tail.size())
            tail.add(idx, nrect);
    }
    public boolean isColliding(Rectangle hitbox){
        for(Rectangle rect: tail){
            if(rect.intersects(hitbox))
                return true;
        }
        return false;
    }
    private Rectangle getCollider(Rectangle rect){
        for(Rectangle r: tail){
            if(r.intersects(rect))
                return r;
        }
        return null;
    }
    private Color getSlickColor(PlayerColor color) {
        switch (color) {
        case CYAN:
            return Color.cyan;
        case GREEN:
            return Color.green;
        case MAGENTA:
            return Color.magenta;
        case ORANGE:
            return Color.orange;
        }
        throw new RuntimeErrorException(null, "Unknown PlayerColor");
    }

    public void update(float centerX, float centerY, int direction, float dist) {
        if (direction != this.direction) {
            this.direction = direction;
            addRectangle(centerX, centerY);
        }
        updateRectangle(dist);
    }

    private void updateRectangle(float dist) {
        Rectangle rect = current();
        switch (direction) {
        case Player.NORTH:
            rect.setY(rect.getY() - dist);
        case Player.SOUTH:
            rect.setHeight(rect.getHeight() + dist);
            break;
        case Player.WEST:
            rect.setX(rect.getX() - dist);
        case Player.EAST:
            rect.setWidth(rect.getWidth() + dist);
        }
    }
}