package com.tron.entities;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class BasicEntity {
    protected EntityInterface anim;
    protected Vector2f position = new Vector2f();

    protected BasicEntity(EntityInterface anim) {
        this.anim = anim;
    }

    protected BasicEntity(EntityInterface anim, float x, float y) {
        this(anim);
        position.set(x, y);
    }

    protected BasicEntity(EntityInterface anim, Vector2f position) {
        this(anim, position.x, position.y);
    }

    public float getCenterX() {
        return position.x + getWidth() / 2;
    }

    public float getCenterY() {
        return position.y + getHeight() / 2;
    }

    public int getHeight() {
        return anim.getHeight();
    }

    public int getWidth() {
        return anim.getWidth();
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        anim.draw(position, container, game, g);
    }

    public void setDrawable(EntityInterface anim) {
        this.anim = anim;
    }

    public void setX(float x) {
        position.x = x;
    }

    public void setY(float y) {
        position.y = y;
    }

    public void update(GameContainer container, StateBasedGame game, int delta) {
        anim.update(position, container, game, delta);
    }
}
