package com.tron.entities;

public interface PowerUpInterface {
	
	default public void before(Player victim, int delta) { }
	default public void update(Player victim, int delta) { }
	default public void after(Player victim, int delta) { } 
	
}
