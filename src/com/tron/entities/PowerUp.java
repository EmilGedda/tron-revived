package com.tron.entities;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;

public class PowerUp extends BasicEntity {

    public enum Type {
        POSITIVE,
        NEGATIVE
    }
    private final static String particledown = "Resources/powerdown.png";
    private final static String particleup = "Resources/powerup.png";

    private PowerUpInterface action;
    private int dur;
    private boolean taken;
    private boolean isLive;
    private Player victim;
    private Rectangle hitbox;
    private ParticleSystem system;
    private ConfigurableEmitter idleemitter;

    private final static String powerupxml = "Resources/powerup.xml";
    private final static String powerdownxml = "Resources/powerdown.xml";
    private Image particle;

    protected PowerUp(EntityInterface anim, Type t, float x, float y, int dur, PowerUpInterface action) {
        super(anim, x, y);
        this.dur = dur;
        this.action = action;
        hitbox = new Rectangle(x, y, anim.getWidth(), anim.getHeight());
        try{
            if(t == Type.POSITIVE){
                particle = new Image(particleup);
                idleemitter = ParticleIO.loadEmitter(powerupxml);
            } else {
                particle = new Image(particledown);
                idleemitter = ParticleIO.loadEmitter(powerdownxml);                
            }
            
            system = new ParticleSystem(particle, 1500);
            idleemitter.setPosition(x + getWidth()/2, y, true);
            system.setRemoveCompletedEmitters(false);
            system.addEmitter(idleemitter);		
        } catch(Exception e){ 
            e.printStackTrace();
        }
    }	

    public void take(Player p){
        victim = p;
        isLive = taken = true;		
        action.before(p, 0);
        idleemitter.wrapUp();
    }

    public Rectangle getHitbox(){
        return hitbox;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {		
        if(!taken){
            super.render(container, game, g);			
        }
        system.render();
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        super.update(container, game, delta);
        system.update(delta);
        if(taken && dur > 0){
            action.update(victim, delta);
            dur -= delta;
            if(isLive && dur <= 0){
                isLive = false;
                action.after(victim, delta);
            }
        }
    }
}