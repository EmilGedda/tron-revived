package com.tron.entities;

import org.newdawn.slick.Image;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;

import com.tron.entities.PowerUp.Type;

public class Explosion {
    private ParticleSystem system;
    private ConfigurableEmitter emitter;

    private final static String xml = "Resources/explosion.xml";
    private final static String particle = "Resources/explosionparticleH.png";

    private boolean moved;
    private float x, y;
    public Explosion(float x, float y){
        try{
            emitter = ParticleIO.loadEmitter(xml);
            Image particle = new Image(Explosion.particle);
            this.x = x; this.y = y;
            system = new ParticleSystem(particle, 1500);
            emitter.setPosition(x, y, false);
            system.addEmitter(emitter);     
        } catch(Exception e){ 
            e.printStackTrace();
        }
    }

    public void render(){
        system.render();
    }

    public void update(int delta){
        system.update(delta);
        if(!moved){
            emitter.setPosition(x, y, true);
            moved = true;
        }
    }

}
