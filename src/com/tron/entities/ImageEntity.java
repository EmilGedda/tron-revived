package com.tron.entities;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class ImageEntity implements EntityInterface{

    private Image img;
    
    public ImageEntity(Image img) {
        this.img = img;
    }
    
    @Override
    public void draw(Vector2f position, GameContainer container, StateBasedGame game, Graphics g) {
        img.draw(position.x, position.y);        
    }

    @Override
    public int getHeight() {
        return img.getHeight();
    }

    @Override
    public int getWidth() {
        return img.getWidth();
    }
    
}
