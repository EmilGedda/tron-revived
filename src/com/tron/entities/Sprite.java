package com.tron.entities;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class Sprite implements EntityInterface {
    Animation anim;

    public Sprite(Animation anim) {
        this.anim = anim;
    }

    @Override
    public void draw(Vector2f position, GameContainer container, StateBasedGame game, Graphics g) {
        anim.draw(position.x, position.y);
    }

    @Override
    public int getHeight() {
        return anim.getHeight();
    }

    // Just to make Animation implement EntityInterface.
    @Override
    public int getWidth() {
        return anim.getWidth();
    }

    public void setCurrentFrame(int index) {
        anim.setCurrentFrame(index);
    }

    @Override
    public void update(Vector2f position, GameContainer container, StateBasedGame game, int delta) {
        anim.update(delta);
    }
}
