package com.tron.entities;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class RocketLauncher {
    private ArrayList<Rocket> rockets = new ArrayList<Rocket>();
    private static Image[] rocketimgs;
    private int timer = 0;

    public RocketLauncher() {
        try {
            if(rocketimgs == null) {
                Image rocket = new Image("Resources/rocket.png");
                rocketimgs = new Image[] {
                        rocket, rocket.copy(),
                        rocket.copy(), rocket.copy(),
                };
                for(int i = 1; i < rocketimgs.length; i++)
                    rocketimgs[i].setRotation((4-i)*90);
            }
        } catch (SlickException e) {
            e.printStackTrace();
        };
    }

    public void FIRINMAHLAZOR(int direction, float playerspeed, float x, float y){
        if(timer <= 0){
        rockets.add(new Rocket(
                new ImageEntity(rocketimgs[direction]),
                x, y, playerspeed + 4, direction
                ));
        timer = 5000;
        }
    }

    public void handleWallCollision(Rectangle rect){
        for(Rocket r : rockets)
            if(r.isAlive())
                r.handleWallCollision(rect);        
    }
    
    public void handleCollision(Player owner, Player enemy){
        if(enemy.isAlive())
            for(Rocket r : rockets)
                r.handleCollision(owner, enemy);
    }
    
    public void update(GameContainer c, StateBasedGame game, int delta){
        Iterator<Rocket> it = rockets.iterator();
        while(it.hasNext()){
            Rocket r = it.next();
            r.update(c, game, delta); 
            if(!r.isAlive())
                it.remove();            
        }
        timer -= delta;
    }

    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for(Rocket r : rockets)
            r.render(container, game, g);
    }

}
