package com.tron.entities;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public interface EntityInterface {
    public void draw(Vector2f position, GameContainer container, StateBasedGame game, Graphics g);

    int getHeight();

    int getWidth();

    default void update(Vector2f position, GameContainer container, StateBasedGame game, int delta) {

    }
}
