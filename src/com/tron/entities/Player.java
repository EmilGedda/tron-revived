package com.tron.entities;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import com.sun.org.apache.bcel.internal.generic.GETSTATIC;
import com.tron.input.InputManager;
import com.tron.input.InputManager.Action;
import com.tron.menu.PlayerSelection.InputType;
import com.tron.sound.SoundHandler;
import com.tron.sound.SoundHandler.SoundEffect;
import com.tron.states.GameState;

public class Player extends BasicEntity {

    public enum PlayerColor {
        CYAN, GREEN, ORANGE,  MAGENTA
    }

    public static Player CreatePlayer(PlayerColor playercolor, GameContainer gc, InputType type) {
        try {
            Image bike = new Image("Resources/bike_" + playercolor.toString() + ".png");
            Image[] imgs = new Image[] {
                    bike, bike.copy(),
                    bike.copy(), bike.copy(),
            };
            for(int i = 1; i < imgs.length; i++)
                imgs[i].setRotation((4-i)*90);

            Animation anim = new Animation(imgs, 100000);
            anim.stop();
            Sprite spr = new Sprite(anim);
            float[] pos = getStartingPos(playercolor);
            return new Player(gc, spr, pos[0], pos[1], playercolor, type);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private static float[] getStartingPos(PlayerColor color){
        switch(color){
        case CYAN:
            return new float[]{1920/2F, 950F};
        case GREEN:
            return new float[]{1800, 470};
        case MAGENTA:
            return new float[]{80, 430F};
        case ORANGE:
            return new float[]{1920/2F - 20, 100F};

        }
        return null;
        
    }
    private Tail tail;
    private int direction = NORTH;
    private float speed = 3;
    private ArrayList<PowerUp> powerups = new ArrayList<PowerUp>();
    private InputManager input;
    private Rectangle hitbox;
    private PlayerColor clr;
    private RocketLauncher launcher = new RocketLauncher();
    private boolean dead;

    public void Kill(){
        SoundHandler.GetHandler().PlaySound(SoundEffect.CRASH);
        GameState.addExplosion(getCenterX(), getCenterY());
        direction = clr.ordinal();
        tail.clear();
        this.dead = true;
    }

    public boolean isAlive(){
        return !dead;
    }
    public Tail getTail(){
        return tail;
    }

    public Rectangle getHitbox(){
        return hitbox;
    }
    
    public PlayerColor getColor(){
        return clr;
    }
    
    public void setDirection(int i){
        direction = i % 4;
        if (direction < 0)
            direction = EAST;
        updateRotation();
    }
    
    protected Player(GameContainer c, EntityInterface anim, float x, float y, PlayerColor color, InputType type) {
        super(anim, x, y);   
        clr = color;
        tail = new Tail(getCenterX(), getCenterY(), 0, color);
        input = new InputManager(c, type, color);
        hitbox = new Rectangle(x, y, 5, 5);
        direction = color.ordinal();
        updateRotation();
        input.addEvent(Action.TURNLEFT, () -> {
            direction = (direction + 1) % 4;
            updateRotation();
        });

        input.addEvent(Action.TURNRIGHT, () -> {
            direction--;
            if (direction < 0)
                direction = EAST;
            updateRotation();
        });

        input.addEvent(Action.FIRE, () -> {
            float px = 0, py = 0;
            switch (direction) {
            case NORTH:
                px = getX() + getWidth()/2; py = getY() - 5;
                break;
            case SOUTH:
                px = getX() + getWidth()/2; py = getY() + getHeight() + 5;
                break;
            case WEST:
                px = getX() - 10; py = getY() + getHeight()/2;
                break;
            case EAST:
                px = getX() + getWidth() + 10; py = getY() + getHeight()/2;
                break;
            }
            launcher.FIRINMAHLAZOR(direction, speed, px, py);
        });

        input.addEvent(Action.BOOST, () -> {
            System.out.println("Swoosh!");
        });

        input.addEvent(Action.MENUGOBACK, () -> {
            System.out.println("Retreat!");
        });
    }
    
    public void pickUpPowerUp(PowerUp p){
    	p.take(this);
    	powerups.add(p);
    }
    public void removePowerUp(PowerUp p){
    	powerups.remove(p);
    }
    public void setSpeed(float speed){
    	this.speed  = speed;
    }
    
    public float getSpeed(){
    	return speed;
    }
    
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        if(!isAlive()) return;
        tail.draw(g);
        launcher.render(container, game, g);
        super.render(container, game, g);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        if(!isAlive()) return;
        input.pollPlayerActions();
        launcher.update(container, game, delta);
        tail.update(getCenterX(), getCenterY(), direction, (speed * delta) / 10F);
        updatePlayerPosition( (speed * delta) / 10F);
        updatehitBox();
        for(PowerUp p : powerups)
            p.update(container, game, delta);
        super.update(container, game, delta);  
    }      
    public boolean collidesWithWall(Rectangle rect){
        launcher.handleWallCollision(rect);
        return hitbox.intersects(rect);
    }
    public boolean collidesWith(Player enemy){
        launcher.handleCollision(this, enemy);
        return enemy.isAlive() && enemy.getTail().isColliding(hitbox);
    }

    public int getDirection(){
        return direction;
    }

    public final static int NORTH = 0;
    public final static int WEST = 1;
    public final static int SOUTH = 2;
    public final static int EAST = 3;

    private void updatehitBox(){
        switch (direction) {
        case NORTH:
            hitbox.setLocation(getCenterX() - hitbox.getWidth()/2, position.y);
            break;
        case WEST:
            hitbox.setLocation(position.x, getCenterY() - hitbox.getHeight()/2);
            break;
        case SOUTH:
            hitbox.setLocation(getCenterX() - hitbox.getWidth()/2, position.y + getHeight() - hitbox.getHeight());
            break;
        case EAST:
            hitbox.setLocation(position.x + getWidth() - hitbox.getWidth(), getCenterY() - hitbox.getHeight()/2);
            break;
        }
    }

    private void updatePlayerPosition(float dist) {
        switch (direction) {
        case NORTH:
            position.y -= dist;
            break;
        case WEST:
            position.x -= dist;
            break;
        case SOUTH:
            position.y += dist;
            break;
        case EAST:
            position.x += dist;
            break;
        }
    }

    private void updateRotation() { // Rotate around the center;
        float lastX = getCenterX(), lastY = getCenterY();
        ((Sprite) anim).setCurrentFrame(direction);
        position.y -= getCenterY() - lastY;
        position.x -= getCenterX() - lastX;
    }
}
