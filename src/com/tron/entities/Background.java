package com.tron.entities;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Background {
	
	ArrayList<Tile> tiles = new ArrayList<>();
	
	public Background(ArrayList<Tile> tiles) {
		this.tiles = tiles;
	}

	public static Background CreateBackground(String path) {
		try {
			ArrayList<Tile> tempTiles = new ArrayList<>();
            Image imgs = new Image(path);
            for(int x = imgs.getWidth(); x < 1920 - imgs.getWidth(); x += imgs.getWidth()) {
            	for(int y = imgs.getWidth(); y < 1080 - imgs.getWidth(); y += imgs.getHeight()) {
            		tempTiles.add(new Tile(imgs, x, y));
   		     	}
            }
            return new Background(tempTiles);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        return null;
	}
	
	public void draw(GameContainer container, StateBasedGame game, Graphics g) {
		for(Tile t : tiles)
		    t.render(container, game, g);
	}
}