package com.tron.entities;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

public class Tile extends BasicEntity {
    Image img;

	public Tile(Image img, int x, int y) {
		super(null, x, y);
	    this.img = img;
	}
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) {
	    img.draw(getX(), getY());
	}
}
