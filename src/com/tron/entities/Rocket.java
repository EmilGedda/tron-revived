package com.tron.entities;

import java.io.File;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.sound.SoundHandler;
import com.tron.sound.SoundHandler.SoundEffect;
import com.tron.states.GameState;

public class Rocket extends BasicEntity {

    private ParticleSystem system;
    private ConfigurableEmitter emitter;
    private int direction;
    private float speed;
    private int life = 5000;
    private Rectangle hitbox;

    public boolean isAlive(){
        return life >= 0;
    }

    public Rocket(EntityInterface anim, float x, float y, float speed, int direction) {
        super(anim, x, y);
        this.speed = speed;
        this.direction = direction;
        this.hitbox = new Rectangle(x, y, 20, 20);
        try {
            SoundHandler.GetHandler().PlaySound(SoundEffect.FIRE);
            Image img = new Image("Resources/particle.png");
            system = new ParticleSystem(img, 1500);
            File xmlFile = new File("Resources/fire.xml");
            emitter = ParticleIO.loadEmitter(xmlFile);
            emitter.useAdditive = true;
            emitter.angularOffset.setValue(direction * 90);
            system.addEmitter(emitter);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public boolean handleWallCollision(Rectangle rect){
        if(speed <= 0) return false;
        if(hitbox.intersects(rect)){
            destroy(SoundEffect.HIT);
            return true;
        }
        return false;
    }
    public boolean handleCollision(Player owner, Player enemy){
        if(!isAlive()) return false;
        boolean b = hitbox.intersects(enemy.getHitbox());
        if(owner != enemy && b){
            enemy.Kill();
            emitter.wrapUp(); 
            speed = 0;
        }
        float px = position.x-owner.getCenterX();
        float py = position.y-owner.getCenterY();
        if(speed != 0 && enemy.getTail().isColliding(hitbox) && Math.sqrt(px*px + py*py) > 40){
            destroy(SoundEffect.HIT);
            enemy.getTail().explode(enemy, this);
        }
        return b;
    }

    private void destroy(SoundEffect effect){
        GameState.addExplosion(getCenterX(), getCenterY());
        emitter.wrapUp(); 
        SoundHandler.GetHandler().PlaySound(effect);
        speed = 0;
    }
    
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        system.render();
    }

    public Rectangle getHitbox(){
        return hitbox;
    }
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        emitter.setPosition(position.x, position.y, false);
        hitbox.setCenterX(position.x);
        hitbox.setCenterY(position.y);
        system.update(delta);
        updatePosition(delta * speed/10F);
        super.update(container, game, delta);
        life -= delta;
    }

    private void updatePosition(float dist) {
        switch (direction) {
        case Player.NORTH:
            position.y -= dist;
            break;
        case Player.WEST:
            position.x -= dist;
            break;
        case Player.SOUTH:
            position.y += dist;
            break;
        case Player.EAST:
            position.x += dist;
            break;
        }
    }
}
