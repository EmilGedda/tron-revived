package com.tron.input;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.newdawn.slick.Input;

import com.tron.entities.Player.PlayerColor;
import com.tron.input.InputManager.Action;

public class KeyboardInputHandler extends InputHandler{
    private HashMap<Action, List<Integer>> mapping = new HashMap<Action, List<Integer>>();

    public KeyboardInputHandler(Input input, PlayerColor player) {
        super(input, player);
       init();
    }

    @Override
    public boolean isActivated(Action action) {
        List<Integer> keys = mapping.get(action);
        for(Integer key : keys){
            if(input.isKeyPressed(key))
                return true;
        }
        return false;
    }

    private void init(){
        mapping.put(Action.MENUDOWN, Arrays.asList(Input.KEY_S, Input.KEY_DOWN));
        mapping.put(Action.MENUUP, Arrays.asList(Input.KEY_W, Input.KEY_W));
        mapping.put(Action.MENUENTER, Arrays.asList(Input.KEY_ENTER));
        mapping.put(Action.MENUGOBACK, Arrays.asList(Input.KEY_ESCAPE, Input.KEY_RETURN));
        mapping.put(Action.TURNLEFT, getPlayerMapping(player, Action.TURNLEFT));
        mapping.put(Action.TURNRIGHT, getPlayerMapping(player, Action.TURNRIGHT));
        mapping.put(Action.FIRE, getPlayerMapping(player, Action.FIRE));
        mapping.put(Action.BOOST, getPlayerMapping(player, Action.BOOST));
    }
    
    private static List<Integer> getPlayerMapping(PlayerColor player, Action action){ //Beautiful.
        switch (player) {
        case CYAN:
            switch (action){
            case TURNLEFT:
                return Arrays.asList(Input.KEY_A);
            case TURNRIGHT:
                return Arrays.asList(Input.KEY_D);
            case FIRE:                
                return Arrays.asList(Input.KEY_Q);
            case BOOST:
                return Arrays.asList(Input.KEY_E);
            default:
                throw new RuntimeErrorException(null, "Invalid player mapping");
            }
        case ORANGE:
            switch (action){
            case TURNLEFT:
                return Arrays.asList(Input.KEY_J);
            case TURNRIGHT:
                return Arrays.asList(Input.KEY_L);
            case FIRE:                
                return Arrays.asList(Input.KEY_U);
            case BOOST:
                return Arrays.asList(Input.KEY_O);
            default:
                throw new RuntimeErrorException(null, "Invalid player mapping");
            }
        case GREEN:
            switch (action){
            case TURNLEFT:
                return Arrays.asList(Input.KEY_F);
            case TURNRIGHT:
                return Arrays.asList(Input.KEY_H);
            case FIRE:                
                return Arrays.asList(Input.KEY_R);
            case BOOST:
                return Arrays.asList(Input.KEY_Y);
            default:
                throw new RuntimeErrorException(null, "Invalid player mapping");
            }
        case MAGENTA:
            switch (action){
            case TURNLEFT:
                return Arrays.asList(Input.KEY_LEFT);
            case TURNRIGHT:
                return Arrays.asList(Input.KEY_RIGHT);
            case FIRE:                
                return Arrays.asList(Input.KEY_UP);
            case BOOST:
                return Arrays.asList(Input.KEY_DOWN);
            default:
                throw new RuntimeErrorException(null, "Invalid player mapping");
            }
        default:
            throw new RuntimeErrorException(null, "Invalid playercolor");
        }
    }
}
