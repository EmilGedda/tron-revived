package com.tron.input;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;

import com.tron.entities.Player.PlayerColor;
import com.tron.menu.PlayerSelection.InputType;

public class InputManager {

    private KeyboardInputHandler keyboard;
    private ControllerInputHandler controller;
    private ArrayList<ArrayList<Runnable>> events;
    private Action[] actions = Action.values();

    public InputManager(GameContainer c, InputType type, PlayerColor player){
        if(type.toString().equals("Keyboard"))
            keyboard = new KeyboardInputHandler(c.getInput(), player);
        if(type.toString().contains("Controller"))
            controller = new ControllerInputHandler(c.getInput(), player, type);
        events = new ArrayList<ArrayList<Runnable>>(actions.length);
        for(int i = 0; i < actions.length; i++)
            events.add(new ArrayList<Runnable>());        
    }

    public void addEvent(Action action, Runnable event){
        if(event != null)            
            events.get(action.ordinal()).add(event);
    }

    public void removeEvent(Action action, Runnable event){
        events.get(action.ordinal()).remove(event);
    }

    public void poll(){
        for(int i = 0; i < actions.length; i++){
            pollAction(actions[i]);
        }
    }

    public void pollPlayerActions(){
        pollAction(Action.TURNLEFT);
        pollAction(Action.TURNRIGHT);
        pollAction(Action.FIRE);
        pollAction(Action.BOOST);
    }
    public void pollAction(Action action){
        ArrayList<Runnable> event = events.get(action.ordinal());
        if(!event.isEmpty() && ((keyboard != null && keyboard.isActivated(action)) || (controller != null && controller.isActivated(action)))){
            event.forEach(e -> e.run());
        }
    }

    public enum Action {
        MENUUP, MENUDOWN,
        MENUENTER, MENUGOBACK,
        TURNLEFT, TURNRIGHT,
        FIRE, BOOST,        
    }    
}
