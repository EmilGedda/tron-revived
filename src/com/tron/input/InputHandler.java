package com.tron.input;

import org.newdawn.slick.Input;

import com.tron.entities.Player.PlayerColor;
import com.tron.input.InputManager.Action;

public abstract class InputHandler {
    PlayerColor player;
    Input input;
    
    public InputHandler(Input input, PlayerColor player){
        this.input = input;
        this.player = player;
    }
    
    public abstract boolean isActivated(Action action);

}
