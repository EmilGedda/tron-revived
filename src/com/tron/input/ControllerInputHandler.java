package com.tron.input;
import java.util.ArrayList;
import java.util.function.Predicate;

import org.lwjgl.input.Controllers;
import org.lwjgl.input.Controller;
import org.newdawn.slick.Input;

import com.tron.entities.Player.PlayerColor;
import com.tron.input.InputManager.Action;
import com.tron.menu.PlayerSelection.InputType;

public class ControllerInputHandler extends InputHandler{
    private boolean[] lock = new boolean[Action.values().length];
    private static ArrayList<Controller> controllers;
    Controller controller;

    public ControllerInputHandler(Input input, PlayerColor player, InputType type) {
        super(input, player);
        if(controllers == null) init();
        int idx = type.getController() - 1;
        if(idx < controllers.size())
            controller = controllers.get(idx);
    }

    private static void init(){
        controllers = new ArrayList<Controller>();
        for (int i = 0; i < Controllers.getControllerCount(); i++)
        {
            Controller c = Controllers.getController(i);
            if(c.getName().contains("Xbox")){
                controllers.add(c);
            }
        }
    }
    
    public static ArrayList<Controller> Controllers(){
        if(controllers == null) init();
        return controllers;
    }
    @Override
    public boolean isActivated(Action action) {
        if(controller == null) return false;
        switch(action){
        case TURNLEFT:
            return checkLock(action, i -> controller.getPovX() == i, -1);
        case TURNRIGHT:
            return checkLock(action, i -> controller.getPovX() == i, 1);
        case FIRE:
            return checkButton(action, 0);
        case BOOST:
            return checkButton(action, 2);
        case MENUDOWN:
            return checkLock(action, i -> controller.getPovY() == i, 1);            
        case MENUENTER:
            return isActivated(Action.FIRE);
        case MENUGOBACK:
            return checkButton(action, 3);
        case MENUUP:
            return checkLock(action, i -> controller.getPovY() == i, -1);   
        }
        return false;
    }
    
    private boolean checkButton(Action act, int btn){
        return checkLock(act,  i -> controller.isButtonPressed(i), btn);
    }
    
    private boolean checkLock(Action act, Predicate<Integer> pred, int btn){
        int id = act.ordinal();
        boolean b = pred.test(btn);
        if(lock[id] != b)
            return lock[id] = b;        
        return false;
    }

}