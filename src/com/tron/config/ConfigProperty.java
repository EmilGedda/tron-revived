package com.tron.config;

import java.util.Properties;

public enum ConfigProperty {
    RESOLUTIONWIDTH("resolutionwidth", Integer.toString((int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth())),
    RESOLUTIONHEIGHT("resolutionheight", Integer.toString((int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight())),
    PARTICLES("particles", "1"),
    SOUND("sound", "100"),
    FULLSCREEN("fullscreen", "1");

    static Properties getDefaultProperties() { // no modifier = package only.
        Properties prop = new Properties();
        for (ConfigProperty cfgprop : ConfigProperty.values()) {
            prop.put(cfgprop.toString(), cfgprop.defaultValue());
        }
        return prop;
    }

    private final String name;

    private final String defaultVal;

    private ConfigProperty(String s, String defaultVal) {
        name = s;
        this.defaultVal = defaultVal;
    }

    public String defaultValue() {
        return defaultVal;
    }

    @Override
    public String toString() {
        return name;
    }
}
