package com.tron.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.tron.TronMain;

public final class GameConfig {
    /**
     * Loads and returns the config from the default path
     * @return The GameConfig saved in the default filepath
     */
    public static GameConfig getConfig() {
        return getConfig(FILEPATH); // Dra ut i enum?
    }

    /**
     * Loads and returns the config from the specified filepath
     * @param filepath The file from which to load our properties.
     * @return GameConfig in that file, or the default config if an error
     *         occured.
     */
    public static GameConfig getConfig(String filepath) { // lazy-load
        if (singleton != null)
            return singleton;
        Properties prop = loadProperties(filepath);
        return new GameConfig(prop);
    }

    /**
     * Loads a Properties instance from the specified path
     * @param filepath The filepath where the properties are stored.
     * @return The parsed properties, or the default properties if an error
     *         occured.
     */
    private static Properties loadProperties(String filepath) {
        Properties prop = new Properties();
        try (FileInputStream in = new FileInputStream(filepath)) {
            prop.load(in);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TronMain.class.getName()).log(Level.INFO,
                    "Settings file not found, creating a new one.");
            prop = ConfigProperty.getDefaultProperties();
        } catch (SecurityException ex) {
            Logger.getLogger(TronMain.class.getName()).log(
                    Level.WARNING,
                    "Authorization failed when accessing " + FILEPATH
                    + ", running with default settings.");
            prop = ConfigProperty.getDefaultProperties();
        } catch (IOException ex) {
            Logger.getLogger(TronMain.class.getName()).log(
                    Level.WARNING,
                    "IOException occured while reading " + FILEPATH
                    + ", running with default settings.");
            prop = ConfigProperty.getDefaultProperties();
        }
        return prop;
    }

    /**
     * Resets/nullifies the singleton instance, only used for testing.
     */
    static void resetSingleton() {
        singleton = null;
    }

    public static final String FILEPATH = "settings.cfg";

    private static GameConfig singleton;

    private boolean saveOnEdit = true;

    private Properties prop;

    private Properties buffer;

    /**
     * Returns a new instance of GameConfig
     * @param prop The backing storage for our properties
     */
    private GameConfig(Properties prop) {
        this.prop = prop;
        singleton = this;
    }

    public void applyBuffer() {
        if (buffer == null)
            throw new IllegalArgumentException("buffer cannot be null");
        saveOnEdit = true;
        save();
    }

    public void bufferChanges() {
        buffer = new Properties(prop);
        for (Entry<Object, Object> entry : prop.entrySet()) {
            buffer.put(entry.getKey(), entry.getValue());
        }
        saveOnEdit = false;
    }

    public void discardBuffer() {
        prop = buffer;
        // buffer = null;
        saveOnEdit = true;
        save();
    }

    public int getProperty(ConfigProperty property) {
        int val;
        String key = "UNDEFINED";
        try {
            key = prop.getProperty(property.toString());
            val = Integer.parseInt(key);
        } catch (NumberFormatException ex) {
            val = Integer.parseInt(property.defaultValue()); // Default
            // formatting is
            // unit-tested.
            prop.put(property.toString(), property.defaultValue());
            Logger.getLogger(TronMain.class.getName()).log(
                    Level.WARNING,
                    "Error retrieving property \"" + property.toString()
                    + "\", with value " + key, ex);
        }
        return val;
    }

    public void save() {
        save(FILEPATH);
    }

    /**
     * Save our GameConfig to the specified path
     * @param filepath The filepath where the instance should be saved.
     */
    public void save(String filepath) {
        try (FileOutputStream out = new FileOutputStream(filepath)) {
            prop.store(out, null);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    /**
     * Determines if we should save and write to file everytime a proprty gets
     * updated
     * @param value True if we should resave on update.
     */
    public void saveOnEdit(boolean value) {
        saveOnEdit = value;
    }

    public void setProperty(ConfigProperty property, int value) {
        prop.put(property.toString(), Integer.toString(value));
        if (saveOnEdit)
            save();
    }
}
