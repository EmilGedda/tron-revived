package com.tron.menu;

import java.util.Arrays;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.config.ConfigProperty;
import com.tron.config.GameConfig;
import com.tron.states.StateHandler;

public class OptionsMenu extends Menu {
    
    private StateHandler stateHandler = StateHandler.GetHandler();

	private void changeResolution(AppGameContainer gc){
        GameConfig cfg = GameConfig.getConfig();
        try {
            gc.setDisplayMode(
                    cfg.getProperty(ConfigProperty.RESOLUTIONWIDTH),
                    cfg.getProperty(ConfigProperty.RESOLUTIONHEIGHT),
                    cfg.getProperty(ConfigProperty.FULLSCREEN) == 1 ? true : false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BooleanSelectable[] getBooleanSelector(ConfigProperty prop) {
        GameConfig cfg = GameConfig.getConfig();
        boolean b = cfg.getProperty(prop) == 1 ? true : false;
        return new BooleanSelectable[] { new BooleanSelectable(b, prop),
                new BooleanSelectable(!b, prop) };
    }

    @Override
    public int getID() {
        return StateHandler.State.OptionsMenu.ordinal();
    }

    private int getResolutionIndex() {
        GameConfig cfg = GameConfig.getConfig();
        ResolutionSelectable current = new ResolutionSelectable(
                cfg.getProperty(ConfigProperty.RESOLUTIONWIDTH),
                cfg.getProperty(ConfigProperty.RESOLUTIONHEIGHT));
        for (int i = 0; i < defres.length; i++) {
            if (current.hashCode() == defres[i].hashCode())
                return i;
        }
        return 0;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException{
        super.init(container, game);
        GameConfig cfg = GameConfig.getConfig();
        cfg.bufferChanges();
        int idx = getResolutionIndex();

        BooleanSelectable[] particles = getBooleanSelector(ConfigProperty.PARTICLES);
        BooleanSelectable[] sound = getBooleanSelector(ConfigProperty.SOUND);
        BooleanSelectable[] fullscreen = getBooleanSelector(ConfigProperty.FULLSCREEN);

        TextItemSelector<?>[] selectors = new TextItemSelector[]{
                new TextItemSelector<Integer>(defres, idx, TextItem.defaultMenuItem(defres[idx].toString()), center + 60 + 220, 420),
                new TextItemSelector<Boolean>(particles, TextItem.defaultMenuItem(particles[0].toString()), center + 60 + 220, 480),
                new TextItemSelector<Boolean>(sound, TextItem.defaultMenuItem(sound[0].toString()), center + 60 + 220, 540),
                new TextItemSelector<Boolean>(fullscreen, TextItem.defaultMenuItem(fullscreen[0].toString()), center + 60 + 220, 600),
        };

        MenuItem[] i = new MenuItem[] {
                new MenuItem(TextItem.defaultMenuItem("Resolution: "), center - 50, 420),
                new MenuItem(TextItem.defaultMenuItem("Particles"), center - 50, 480),
                new MenuItem(TextItem.defaultMenuItem("Sound"), center - 50, 540),
                new MenuItem(TextItem.defaultMenuItem("Fullscreen"), center - 50, 600),
                
            new MenuItem(TextItem.defaultMenuItem("Save"), (a, b, c) -> {
                    cfg.applyBuffer();
                    AppGameContainer gc = (AppGameContainer) container;
                    changeResolution(gc);
                    try {
						stateHandler.changeState(StateHandler.State.StartMenu, game);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            }, center + 30, 690),

            new MenuItem(TextItem.defaultMenuItem("Cancel"), (a, b, c) -> {
            		try {
            			stateHandler.changeState(StateHandler.State.StartMenu, game);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    cfg.discardBuffer();
                    for (TextItemSelector<?> selector : selectors)
                        selector.reset();
            }, center + 310, 690)                
        };

        items.addAll(Arrays.asList(i));
        items.addAll(Arrays.asList(selectors));
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        if (container.getInput().isKeyDown(Input.KEY_ESCAPE)) {
            GameConfig.getConfig().discardBuffer();
            try {
				stateHandler.changeState(StateHandler.State.StartMenu, game);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        super.update(container, game, delta);
    }

    ResolutionSelectable[] defres = {
            new ResolutionSelectable(2560, 1600),
            new ResolutionSelectable(2560, 1440),
            new ResolutionSelectable(2048, 1152),
            new ResolutionSelectable(1920, 1200),
            new ResolutionSelectable(1920, 1080),
            new ResolutionSelectable(1680, 1050),
            new ResolutionSelectable(1600, 1200),
            new ResolutionSelectable(1600, 900),
            new ResolutionSelectable(1536, 864),
            new ResolutionSelectable(1440, 900),
            new ResolutionSelectable(1400, 1050),
            new ResolutionSelectable(1366, 768),
            new ResolutionSelectable(1280, 1024),
            new ResolutionSelectable(1280, 960),
            new ResolutionSelectable(1280, 800),
            new ResolutionSelectable(1280, 768),
            new ResolutionSelectable(1280, 720),
            new ResolutionSelectable(1152, 864),
            new ResolutionSelectable(1093, 614),
            new ResolutionSelectable(1024, 768),
            new ResolutionSelectable(800, 600),
    };

}
