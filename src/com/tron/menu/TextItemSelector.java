package com.tron.menu;


public class TextItemSelector<T> extends MenuItem {
    private SelectableTextItem<T>[] items;
    private int defaultIndex;
    private int current;

    public TextItemSelector(SelectableTextItem<T>[] items, int startIndex, TextItem anim, float x, float y) {
        super(anim, x, y);
        this.items = items;
        defaultIndex = current = startIndex;
        event = new MouseActionInterface() {
            @Override
            public void mouseEnter(int x, int y) {
                int lastWidth = anim.getWidth(), lastHeight = anim.getHeight();
                anim.setFontSize(anim.getFontSize() + 5);
                updatePosition(lastWidth, lastHeight);
            }

            @Override
            public void mouseLeave(int x, int y) {
                int lastWidth = anim.getWidth(), lastHeight = anim.getHeight();
                anim.setFontSize(anim.getFontSize() - 5);
                updatePosition(lastWidth, lastHeight);
            }

            @Override
            public void mousePressed(int button, int x, int y) {
                int lastWidth = anim.getWidth(), lastHeight = anim.getHeight();
                current = (1 + current) % items.length; //TODO: rightclick reverse.
                anim.setText(displayValue());
                updatePosition(lastWidth, lastHeight);
                forceUpdateEmitter();
                items[current].onSelected();
            }

            private void updatePosition(int lastWidth, int lastHeight) {
                int dw = anim.getWidth() - lastWidth;
                int dh = anim.getHeight() - lastHeight;
                position.set(getX() - dw / 2, getY() - dh / 2);
            }

        };
    }
    
    public TextItemSelector(SelectableTextItem<T>[] items, TextItem anim, float x, float y) {
        this(items, 0, anim, x, y);
    }
    
    @SuppressWarnings("unchecked")
    public TextItemSelector(SelectableTextItem<T> items, TextItem anim, float x, float y) {
        this(new SelectableTextItem[]{items}, 0, anim, x, y);
    }

    public String displayValue() {
        return items[current].toString();
    }

    public T[] getSelected() {
        return items[current].getItems();
    }

    public void reset() {
        current = defaultIndex;
        ((TextItem) anim).setText(displayValue());
    }
}