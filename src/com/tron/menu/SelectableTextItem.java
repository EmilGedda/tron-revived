package com.tron.menu;

public interface SelectableTextItem<T> {
    public T[] getItems();

    void onSelected();

    @Override
    String toString();
}
