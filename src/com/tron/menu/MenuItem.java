package com.tron.menu;

import java.io.File;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.entities.BasicEntity;
import com.tron.entities.EntityInterface;

public class MenuItem extends BasicEntity{

    private ParticleSystem system;
    private ConfigurableEmitter emitter;
    private Vector2f lastPosition = new Vector2f(-10, -10);
    private Color highlighted = new Color(127, 127, 127);
    private boolean selected;
    protected MouseActionInterface event;

    public MenuItem(EntityInterface anim, float x, float y) {
        this(anim, null, x, y);
    }

    public MenuItem(EntityInterface anim, MouseActionInterface event, float x, float y) {
        super(anim, x, y);
        this.event = event;
        try {
            Image img = new Image("Resources/particle.png");
            system = new ParticleSystem(img, 1500);
            File xmlFile = new File("Resources/textbg.xml");
            emitter = ParticleIO.loadEmitter(xmlFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        emitter.useAdditive = true;
        system.addEmitter(emitter);
        setEmitterLife(1);
    }
        
    public boolean isSelected(){
        return selected;
    }
    public void Select(){
        selected = true;
        event.mouseEnter(0, 0);
        setEmitterLife(1200);
    }
    
    public void Deselect(){
         selected = false;
        event.mouseLeave(0,0);
        setEmitterLife(1);
    }
    
    public boolean Selectable(){
        return event != null;
    }
    public void Click(){
        event.mousePressed(0, 0,0);
    }
    public void forceUpdateEmitter(){
        position.x += 0.1;
        int width = anim.getWidth(), height = anim.getHeight();
        updateEmitterPosition(position, width, height);
    }
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        Input in = container.getInput();
        int x = in.getMouseX(), y = in.getMouseY(); 
        int width = anim.getWidth(), height = anim.getHeight();
        updateEmitterPosition(position, width, height);
        system.update(delta); 
        if(event != null){
            if(x > position.x && x < position.x + width && y > position.y && y < position.y + height && !selected)
                Select();
            else if (selected && (x < position.x || x > position.x + width || y < position.y || y > position.y + height))
                Deselect();            
            if(selected && in.isMousePressed(Input.MOUSE_LEFT_BUTTON))
                event.mousePressed(Input.MOUSE_LEFT_BUTTON, x, y);
        }
        super.update(container, game, delta);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        system.render();
        if(anim instanceof TextItem){
            TextItem anim = (TextItem)this.anim;
            if(selected) anim.draw(position, highlighted);
            else anim.draw(position);
        } else
            anim.draw(position, container, game, g);
    }

    private void updateEmitterPosition(Vector2f position, int width, int height){
        if(!position.equals(lastPosition)){
            emitter.setPosition(position.x  + width/2F, position.y + height/2F, false);
            emitter.xOffset.setMax(width/2F + 10); emitter.xOffset.setMin(-width/2F - 10);
            emitter.spawnCount.setMax(width*height/500F);
            lastPosition.set(position);
        }
    }
    
    private void setEmitterLife(int life){
        emitter.initialLife.setMax(life);
        emitter.initialLife.setMin(life);
    }
}
