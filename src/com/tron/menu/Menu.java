package com.tron.menu;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.entities.Player.PlayerColor;
import com.tron.input.InputManager;
import com.tron.input.InputManager.Action;
import com.tron.menu.PlayerSelection.InputType;
import com.tron.sound.SoundHandler;
import com.tron.sound.SoundHandler.Track;

public abstract class Menu extends BasicGameState {

    protected ArrayList<MenuItem> items = new ArrayList<MenuItem>();
    protected MenuItem header;
    protected float center;
    protected InputManager input;
    protected int selected = -1;
    private int lastX = -1, lastY = 1;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        input = new InputManager(container, InputType.KEYBOARDANDCONTROLLER, PlayerColor.CYAN);
        header = new MenuItem(TextItem.createHeader(), 540, 150);
        center = -header.getWidth() / 2F + 1920/2F;
        header.setX(center);
        SoundHandler.GetHandler().PlayMusic(Track.MENU);

        input.addEvent(Action.MENUDOWN, () -> {
            do {
                selected = (selected + 1) % items.size();
            } while(!items.get(selected).Selectable());
        });

        input.addEvent(Action.MENUUP, () -> {       
            do {
                selected--;
                if(selected < 0) selected = items.size() - 1;
            } while(!items.get(selected).Selectable());
        });

        input.addEvent(Action.MENUENTER, () -> {
            if(selected > -1)
                items.get(selected).Click();
        });
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for (MenuItem item : items) {
            item.render(container, game, g);
        }
        header.render(container, game, g);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        for (int i = 0; i < items.size(); i++) {
            MenuItem item = items.get(i);
            item.update(container, game, delta);
            if(selected != i && item.isSelected()){
                if(selected > -1) items.get(selected).Deselect();
                selected = i;
            }
        }
        input.poll();
        int x = container.getInput().getMouseX();
        int y = container.getInput().getMouseY();
        if(x == lastX && y == lastY){
            if(selected > -1 && !items.get(selected).isSelected()) items.get(selected).Select();                
        } else {
            lastX = x; lastY = y;
            selected = -1;
        }
    }

}
