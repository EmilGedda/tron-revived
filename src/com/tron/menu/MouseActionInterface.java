package com.tron.menu;

public interface MouseActionInterface {
    default void mouseEnter(int x, int y) {
    }

    default void mouseLeave(int x, int y) {
    }

    void mousePressed(int button, int x, int y);
}
