package com.tron.menu;

import java.util.ArrayList;
import java.util.Arrays;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.config.ConfigProperty;
import com.tron.input.ControllerInputHandler;
import com.tron.states.StateHandler;
import com.tron.states.StateHandler.State;

public class PlayerSelection extends Menu {
    private static InputType[] inputs = InputType.values();
    private static InputType[] selectedInput = new InputType[4];
    private static int controllers;
    
    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);
        inputs = InputType.values();
        controllers = ControllerInputHandler.Controllers().size();

        TextItemSelector<?>[] selectors = new TextItemSelector[]{
                new TextItemSelector<InputType>(InputSelectable.getAll(), TextItem.defaultMenuItem("Off"), center + 60 + 280, 420),
                new TextItemSelector<InputType>(InputSelectable.getAll(), TextItem.defaultMenuItem("Off"), center + 60 + 280, 480),
                new TextItemSelector<InputType>(InputSelectable.getAll(), TextItem.defaultMenuItem("Off"), center + 60 + 280, 540),
                new TextItemSelector<InputType>(InputSelectable.getAll(), TextItem.defaultMenuItem("Off"), center + 60 + 280, 600),

        };

        MenuItem[] i = new MenuItem[] {
                new MenuItem(TextItem.defaultMenuItem("Cyan:"), center - 50, 420),
                new MenuItem(TextItem.defaultMenuItem("Orange:"), center - 50, 480),
                new MenuItem(TextItem.defaultMenuItem("Green"), center - 50, 540),
                new MenuItem(TextItem.defaultMenuItem("Magenta:"), center - 50, 600),

                new MenuItem(TextItem.defaultMenuItem("GO!"), (a, b, c) -> {
                    int starting = 0;
                    for(int j = 0; j < selectors.length; j++){
                        selectedInput[j] = (InputType)selectors[j].getSelected()[0];
                        if(selectedInput[j] != InputType.OFF)
                            starting++;
                    }
                    if(starting >= 2)
                        StateHandler.GetHandler().changeState(StateHandler.State.Singleplayer, game);
                }, center + 30, 690),

                new MenuItem(TextItem.defaultMenuItem("Cancel"), (a, b, c) -> {
                    StateHandler.GetHandler().changeState(StateHandler.State.StartMenu, game);
                }, center + 310, 690)                
        };

        items.addAll(Arrays.asList(i));
        items.addAll(Arrays.asList(selectors));
    }

    public static InputType[] getSelectedInput(){
        return selectedInput;
    }
    
    @Override
    public int getID() {
        return State.PlayerSelection.ordinal();
    }

    public enum InputType{
        OFF("Off",  -1),
        KEYBOARD("Keyboard", -1),
        CONTROLLER1("Controller 1", 1),
        CONTROLLER2("Controller 2", 2),
        CONTROLLER3("Controller 3", 3),
        CONTROLLER4("Controller 4", 4),      
        KEYBOARDANDCONTROLLER("Keyboard and Controller", 1);

        InputType(String value, int ctrl){
            this.value = value;
            this.ctrl = ctrl;
        }
        public static ArrayList<InputType> getTypes(){
            ArrayList<InputType> t = new ArrayList<InputType>();
            for(int i = 0; i < controllers + 2; i++)
                t.add(inputs[i]);
            return t;
        }
        private String value;
        private int ctrl;
        
        public int getController(){
            return ctrl;
        }
        
        @Override
        public String toString() {
            return value;
        };
    }
}
