package com.tron.menu;

import java.io.File;
import java.util.Arrays;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.tron.states.StateHandler;

public class StartMenu extends Menu {
	
	static StateHandler stateHandler = new StateHandler();
	
    @Override
    public int getID() {
        return StateHandler.State.StartMenu.ordinal();
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);
        MenuItem[] i = new MenuItem[] {
                new MenuItem(TextItem.defaultMenuItem("1. Play"), (a, b, c) -> stateHandler.changeState(StateHandler.State.PlayerSelection, game), center + 140, 450F),
                new MenuItem(TextItem.defaultMenuItem("2. Help"), (a, b, c) -> {
                    try {
                        java.awt.Desktop.getDesktop().open(new File("README.txt"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    }, center + 140, 510F),
                new MenuItem(TextItem.defaultMenuItem("3. Options"), (a, b, c) -> stateHandler.changeState(StateHandler.State.OptionsMenu, game), center + 140, 570F),
                new MenuItem(TextItem.defaultMenuItem("4. Exit"), (a, b, c) -> {game.closeRequested();container.exit();}, center + 140, 630F)
        };
        items.addAll(Arrays.asList(i));
    }
}