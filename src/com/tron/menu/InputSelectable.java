package com.tron.menu;

import java.util.ArrayList;

import com.tron.menu.PlayerSelection.InputType;

public class InputSelectable implements SelectableTextItem<InputType> {

    private InputType type;
    public InputSelectable(InputType type) {
        this.type = type;
    }
    @Override
    public InputType[] getItems() {
        return new InputType[]{type};
    }

    @Override
    public void onSelected() {     
    }
    
    public static InputSelectable[] getAll(){
        ArrayList<InputType> inputs = PlayerSelection.InputType.getTypes();
        InputSelectable[] arr = new InputSelectable[inputs.size()];
        for(int i = 0; i < arr.length; i++)
            arr[i] = new InputSelectable(inputs.get(i));
        return arr;
    }
    @Override
    public String toString() {
        return type.toString();
    }
}
