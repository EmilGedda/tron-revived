package com.tron.menu;

import com.tron.config.ConfigProperty;
import com.tron.config.GameConfig;

public class BooleanSelectable implements SelectableTextItem<Boolean> {
    private boolean b;
    private ConfigProperty prop;

    public BooleanSelectable(boolean b, ConfigProperty prop) {
        this.b = b;
        this.prop = prop;
    }

    @Override
    public Boolean[] getItems() {
        return new Boolean[] { b };
    }

    @Override
    public void onSelected() {
        GameConfig.getConfig().setProperty(prop, b ? 1 : 0);
    }

    @Override
    public String toString() {
        return b ? "On" : "Off";
    }

}
