package com.tron.menu;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.newdawn.slick.font.effects.Effect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import com.tron.entities.EntityInterface;

public class TextItem implements EntityInterface{
    private Font backingFont;
    private UnicodeFont font;
    private java.awt.Color regular;
    private float size;
    private String text;   

    public TextItem(String text, String path, float size, java.awt.Color standard) {
        try {
            backingFont = Font.createFont(Font.TRUETYPE_FONT, ResourceLoader.getResourceAsStream(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        regular = standard;
        this.text = text;
        loadFont(size);
    }

    private void loadFont(float size) {
        this.size = size;
        font = new UnicodeFont(backingFont.deriveFont(size));
        font.addAsciiGlyphs();
        @SuppressWarnings("unchecked")
        List<Effect> effects = font.getEffects();
        effects.add(new ColorEffect(regular));
        try {
            font.loadGlyphs();
        } catch (SlickException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void outline(int width, java.awt.Color clr){
        font = new UnicodeFont(backingFont.deriveFont(size));
        font.addAsciiGlyphs();
        @SuppressWarnings("unchecked")
        List<Effect> effects = font.getEffects();
        //effects.add(new ColorEffect(regular));
        effects.add(new OutlineEffect(width, clr));
        try {
            font.loadGlyphs();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
    
    public void setText(String text) {
        this.text = text;
    }  

    public UnicodeFont getFont(){
        return font;
    }
    public float getFontSize(){
        return size;
    }
    
    public void setFontSize(float size){
        loadFont(size);
    }
    @Override
    public int getWidth(){
        return font.getWidth(text);
    }

    @Override
    public int getHeight(){
        return font.getHeight(text);
    }


    @Override
    public void draw(Vector2f position, GameContainer container, StateBasedGame game, Graphics g) {
         font.drawString(position.x, position.y, text);
    }
    public void draw(Vector2f position) {
       font.drawString(position.x, position.y, text);
   }
    public void draw(Vector2f position, Color clr) {
         font.drawString(position.x, position.y, text, clr);
    }
    
    public static TextItem createHeader() {
        return new TextItem("Tron", "Resources/Tr2n.ttf", 150, new java.awt.Color(24, 202, 230));
    }
    public static TextItem defaultMenuItem(String text) {
        return new TextItem(text, "Resources/skirmisher.ttf", 40, new java.awt.Color(230, 230, 230));
    }
}
