package com.tron.menu;

import com.tron.config.ConfigProperty;
import com.tron.config.GameConfig;

public class ResolutionSelectable implements SelectableTextItem<Integer> {
    private int width;
    private int height;
    private String str;

    public ResolutionSelectable(int width, int height) {
        this.width = width;
        this.height = height;
        str = width + " x " + height;
    }

    @Override
    public Integer[] getItems() {
        return new Integer[] { width, height };
    }

    @Override
    public int hashCode() {
        return width * 31 + height;
    }

    @Override
    public void onSelected() {
        GameConfig cfg = GameConfig.getConfig();
        cfg.saveOnEdit(false);
        cfg.setProperty(ConfigProperty.RESOLUTIONWIDTH, width);
        cfg.saveOnEdit(true);
        cfg.setProperty(ConfigProperty.RESOLUTIONHEIGHT, height);
    }

    @Override
    public String toString() {
        return str;
    }
}
