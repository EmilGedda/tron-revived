package com.tron.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestGameConfig {

    GameConfig cfg;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        GameConfig.resetSingleton();
        cfg = GameConfig.getConfig();
        cfg.saveOnEdit(false);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        File file = new File(GameConfig.FILEPATH);
        file.delete();
    }

    /**
     * Test method for {@link com.tron.config.GameConfig#applyBuffer()}.
     */
    @Test
    public void testapplyBuffer() {
        cfg.bufferChanges();
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            cfg.setProperty(props, -1);
        }
        cfg.applyBuffer();
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            assertEquals(cfg.getProperty(props), -1);
        }
    }

    /**
     * Test method for {@link com.tron.config.GameConfig#discardBuffer()}.
     */
    @Test
    public void testdiscardBuffer() {
        cfg.bufferChanges();
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            cfg.setProperty(props, -1);
        }
        cfg.discardBuffer();
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            assertEquals(Integer.toString(cfg.getProperty(props)),
                    props.defaultValue());
        }
    }

    /**
     * Test method for {@link com.tron.config.GameConfig#getConfig()}.
     */
    @Test
    public void testGetConfig() {
        assertEquals(cfg, GameConfig.getConfig());
        assertNotNull(GameConfig.getConfig()); // Multiple retrievals should not
        // return null
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            assertEquals(Integer.toString(cfg.getProperty(props)),
                    props.defaultValue());
        }
        cfg.setProperty(ConfigProperty.SOUND, 100);
        assertEquals(cfg, GameConfig.getConfig());
        cfg = null;
        assertNotNull(GameConfig.getConfig());
    }

    /**
     * Test method for
     * {@link com.tron.config.GameConfig#getProperty(ConfigProperty)}.
     */
    @Test
    public void testGetProperty() {
        for (ConfigProperty props : ConfigProperty.values()) { // Check if
            // default is set
            // at start.
            assertEquals(Integer.toString(cfg.getProperty(props)),
                    props.defaultValue());
        }
    }

    /**
     * Test method for {@link com.tron.config.GameConfig#save()}.
     */
    @Test
    public void testSave() {
        Random rng = new Random();
        ArrayList<Integer> list = new ArrayList<Integer>();
        ConfigProperty[] values = ConfigProperty.values();
        for (int i = 0; i < values.length; i++) {
            list.add(rng.nextInt());
            cfg.setProperty(values[i], list.get(i));
        }
        File conf = new File(GameConfig.FILEPATH);
        assertFalse(conf.exists());
        cfg.save();
        assertTrue(conf.exists());
        assertTrue(conf.length() > 10);
        assertTrue(conf.length() < 10000);
        GameConfig.resetSingleton();
        GameConfig cfg2 = GameConfig.getConfig();
        assertNotEquals(cfg, cfg2);
        for (ConfigProperty props : values) {
            assertEquals(cfg.getProperty(props), cfg2.getProperty(props));
        }
    }

    /**
     * Test method for
     * {@link com.tron.config.GameConfig#SetProperty(ConfigProperty, int)}.
     */
    @Test
    public void testSetProperty() {
        ConfigProperty[] values = ConfigProperty.values();
        for (int i = 0; i < values.length; i++) {
            cfg.setProperty(values[i], i);
        }
        for (int i = 0; i < values.length; i++) {
            assertEquals(cfg.getProperty(values[i]), i);
        }
    }
}