ÄNDRA UPPLÖSNINGEN OCH KÖR I FULLSCREEN.
Kan spelas med 2-4 spelare.

Varje spelare har två styrknappar och en raketknapp.
Styrknapparna ändrar spelarens rikting till vänster eller höger relativt till den nuvarande riktningen.
Raketknappen skjuter en eldboll som kan göra hål i väggar och döda fiender, en raket kan skjutas var femte sekund.

Kontroller:

CYAN: 
	Raketknapp: Q
	Styrknappar: A, D

GREEN: 
	Raketknapp: R
	Styrknappar: F, H

ORANGE: 
	Raketknapp: U
	Styrknappar: J, L

MAGENTA: 
	Raketknapp: Pil upp
	Styrknappar: Pil vänster, Pil höger.

Man kan även köra med en  xbox 360-kontroller vilket rekommenderas för maximal spelupplevelse.
Man styr då med d-pad och skjuter på A-knappen

Powerups:

TURN - NEGATIVE: 
	Visas som en pil som är rund, den får spelaren att slumpmässigt byta riktning.
	Riktigt dödlig.

SPEEDUP - POSITIVE:
	Ökar spelarens hastighet en kort stund.

SPEEDDOWN - NEGATIVE:
	Sänker spelarens hastighet en kort stund

INVISIBLEWALLS - POSITIVE:
	Döljer spelarens alla väggar VISUELLT en kort stund.